<h2>Images</h2>
<p><img src="https://voidspiral.gitlab.io/imghost/fonts/Nakaryons_Gifts/Nakaryons_Hand_Wallpapers_1.jpg"/></p>
<p><img src="https://voidspiral.gitlab.io/imghost/fonts/Nakaryons_Gifts/Nakaryons_Hand_Wallpapers_2.jpg"/></p>
<p><img src="https://voidspiral.gitlab.io/imghost/fonts/Nakaryons_Gifts/Nakaryons_Hand_Wallpapers_3.jpg"/></p>
<p><img src="https://voidspiral.gitlab.io/imghost/fonts/Nakaryons_Gifts/Nakaryons_Hand_Wallpapers_4.jpg"/></p>
<h2>Included Files</h2>
<ul><li>license.txt</li>
<li>NakaryonsGifts.otf</li>
<li>nakaryonsgifts.ttf</li>
<li>NakaryonsGifts.woff</li>
<li>NakaryonsGifts_Bold.otf</li>
<li>nakaryonsgifts_bold.ttf</li>
<li>NakaryonsGifts_Bold.woff</li>
<li>NakaryonsGifts_Italic.otf</li>
<li>nakaryonsgifts_italic.ttf</li>
<li>NakaryonsGifts_Italic.woff</li>
<li>Nakaryons_Hand_Circle_1.png</li>
<li>Nakaryons_Hand_Circle_2.png</li>
<li>Nakaryons_Hand_Circle_3.png</li>
<li>Nakaryons_Hand_Circle_4.png</li>
<li>Nakaryons_Hand_Circle_5.png</li>
<li>Nakaryons_Hand_Wallpapers_1.jpg</li>
<li>Nakaryons_Hand_Wallpapers_2.jpg</li>
<li>Nakaryons_Hand_Wallpapers_3.jpg</li>
<li>Nakaryons_Hand_Wallpapers_4.jpg</li>
</ul>
<h2>License</h2>
<p>This font is licensed under the SIL Open Font License, meaning you can use this font for personal or commercial use.</p><a href = "http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL"><img src="https://voidspiral.gitlab.io/imghost/ofl/PNG/OFLLogoRectBW.png"/></a>
