<h2>Images</h2>
<p><img src="https://voidspiral.gitlab.io/imghost/fonts/Nakaryons_Prose/Prose_types.jpg"/></p>
<h2>Included Files</h2>
<ul><li>license.txt</li>
<li>NakaryonsProse Book Black.otf</li>
<li>NakaryonsProse Book Black.woff</li>
<li>NakaryonsProse Book Bold.otf</li>
<li>NakaryonsProse Book Bold.woff</li>
<li>NakaryonsProse Book Italic.otf</li>
<li>NakaryonsProse Book Italic.woff</li>
<li>NakaryonsProse Book Thin.otf</li>
<li>NakaryonsProse Book Thin.woff</li>
<li>NakaryonsProse Book.otf</li>
<li>NakaryonsProse Book.woff</li>
<li>NakaryonsProse Condensed Black.otf</li>
<li>NakaryonsProse Condensed Black.woff</li>
<li>NakaryonsProse Condensed Bold.otf</li>
<li>NakaryonsProse Condensed Bold.woff</li>
<li>NakaryonsProse Condensed Italic.otf</li>
<li>NakaryonsProse Condensed Italic.woff</li>
<li>NakaryonsProse Condensed Regular.otf</li>
<li>NakaryonsProse Condensed Regular.woff</li>
<li>NakaryonsProse Condensed Thin.otf</li>
<li>NakaryonsProse Condensed Thin.woff</li>
<li>NakaryonsProse Expanded Black.otf</li>
<li>NakaryonsProse Expanded Black.woff</li>
<li>NakaryonsProse Expanded Bold.otf</li>
<li>NakaryonsProse Expanded Bold.woff</li>
<li>NakaryonsProse Expanded Italic.otf</li>
<li>NakaryonsProse Expanded Italic.woff</li>
<li>NakaryonsProse Expanded Regular.otf</li>
<li>NakaryonsProse Expanded Regular.woff</li>
<li>NakaryonsProse Expanded Thin.otf</li>
<li>NakaryonsProse Expanded Thin.woff</li>
<li>NakaryonsProse Ultra Condensed Black.otf</li>
<li>NakaryonsProse Ultra Condensed Black.woff</li>
<li>NakaryonsProse Ultra Condensed Bold.otf</li>
<li>NakaryonsProse Ultra Condensed Bold.woff</li>
<li>NakaryonsProse Ultra Condensed Italic.otf</li>
<li>NakaryonsProse Ultra Condensed Italic.woff</li>
<li>NakaryonsProse Ultra Condensed Thin.otf</li>
<li>NakaryonsProse Ultra Condensed Thin.woff</li>
<li>NakaryonsProse Ultra Condensed.otf</li>
<li>NakaryonsProse Ultra Condensed.woff</li>
<li>NakaryonsProse Wide Black.otf</li>
<li>NakaryonsProse Wide Black.woff</li>
<li>NakaryonsProse Wide Bold.otf</li>
<li>NakaryonsProse Wide Bold.woff</li>
<li>NakaryonsProse Wide Italic.otf</li>
<li>NakaryonsProse Wide Italic.woff</li>
<li>NakaryonsProse Wide Thin.otf</li>
<li>NakaryonsProse Wide Thin.woff</li>
<li>NakaryonsProse Wide.otf</li>
<li>NakaryonsProse Wide.woff</li>
<li>nakaryonsprose_book.ttf</li>
<li>nakaryonsprose_book_black.ttf</li>
<li>nakaryonsprose_book_bold.ttf</li>
<li>nakaryonsprose_book_italic.ttf</li>
<li>nakaryonsprose_book_thin.ttf</li>
<li>nakaryonsprose_condensed_black.ttf</li>
<li>nakaryonsprose_condensed_bold.ttf</li>
<li>nakaryonsprose_condensed_italic.ttf</li>
<li>nakaryonsprose_condensed_regular.ttf</li>
<li>nakaryonsprose_condensed_thin.ttf</li>
<li>nakaryonsprose_expanded_black.ttf</li>
<li>nakaryonsprose_expanded_bold.ttf</li>
<li>nakaryonsprose_expanded_italic.ttf</li>
<li>nakaryonsprose_expanded_regular.ttf</li>
<li>nakaryonsprose_expanded_thin.ttf</li>
<li>nakaryonsprose_ultra_condensed.ttf</li>
<li>nakaryonsprose_ultra_condensed_black.ttf</li>
<li>nakaryonsprose_ultra_condensed_bold.ttf</li>
<li>nakaryonsprose_ultra_condensed_italic.ttf</li>
<li>nakaryonsprose_ultra_condensed_thin.ttf</li>
<li>nakaryonsprose_wide.ttf</li>
<li>nakaryonsprose_wide_black.ttf</li>
<li>nakaryonsprose_wide_bold.ttf</li>
<li>nakaryonsprose_wide_italic.ttf</li>
<li>nakaryonsprose_wide_thin.ttf</li>
<li>Prose_types.jpg</li>
<li>Prose_types.psd</li>
</ul>
<h2>License</h2>
<p>This font is licensed under the SIL Open Font License, meaning you can use this font for personal or commercial use.</p><a href = "http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL"><img src="https://voidspiral.gitlab.io/imghost/ofl/PNG/OFLLogoRectBW.png"/></a>
